package com.example.studenttest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends BaseAdapter {
    List<StudentClass> students;
    LayoutInflater layoutInflater;

    public CustomAdapter(Context context, List<StudentClass> students) {
        this.students = students;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getItem(int i) {
        return students.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_layout, viewGroup, false);
        }
        StudentClass studentClass = (StudentClass) getItem(i);
        ImageView imageView = view.findViewById(R.id.PicOfStudent);
        TextView nameTV = view.findViewById(R.id.Name);
        TextView surnameTV = view.findViewById(R.id.Surname);
        TextView birthdayTV = view.findViewById(R.id.DateBirthday);
        TextView groupTV = view.findViewById(R.id.NumberOfGroup);

        imageView.setImageBitmap(studentClass.getId_PicOfStudent());
        nameTV.setText(studentClass.getName());
        surnameTV.setText(studentClass.getSurname());
        birthdayTV.setText(studentClass.getBirthdayDate());
        groupTV.setText(studentClass.getGroup());

        return view;
    }
}
