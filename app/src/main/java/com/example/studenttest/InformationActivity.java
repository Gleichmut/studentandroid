package com.example.studenttest;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class InformationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);


        Bitmap id_picofstudent =(Bitmap) getIntent().getExtras().get("id_picofstudent");
        String name = getIntent().getStringExtra("name");
        String surname = getIntent().getStringExtra("surname");
        String birthday = getIntent().getStringExtra("birthday");
        String group = getIntent().getStringExtra("group");

        ((ImageView)findViewById(R.id.PicOfStudent)).setImageBitmap(id_picofstudent);
        ((TextView) findViewById(R.id.Name)).setText(name);
        ((TextView) findViewById(R.id.Surname)).setText(surname);
        ((TextView) findViewById(R.id.DateBirthday)).setText(birthday);
        ((TextView) findViewById(R.id.NumberOfGroup)).setText(group);
    }


 /*   public void RegistratoinButtonClick(View view)
    {
      boolean logic = view.getId() == R.id.AddReg

    }

  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK){
            switch (requestCode)
            {
                case Request
             //лист оборажение, добавление в лист,  класс   как с странами
            }
        }
    }*/
}
