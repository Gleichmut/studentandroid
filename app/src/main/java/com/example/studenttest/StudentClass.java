package com.example.studenttest;

import android.graphics.Bitmap;

import java.io.Serializable;

public class StudentClass implements Serializable {
    private Bitmap id_PicOfStudent;
    private String Name;
    private String Surname;
    private String BirthdayDate;
    private String Group;


    public StudentClass(Bitmap id_picOfStudent, String name, String surname, String birthdayDate, String group) {
        this.id_PicOfStudent = id_picOfStudent;
        this.Name = name;
        this.Surname = surname;
        this.BirthdayDate = birthdayDate;
        this.Group = group;
    }

    public Bitmap getId_PicOfStudent() {
        return id_PicOfStudent;
    }

    public String getName() {
        return Name;
    }

    public String getSurname() {
        return Surname;
    }

    public String getBirthdayDate() {
        return BirthdayDate;
    }

    public String getGroup() {
        return Group;
    }
}
