package com.example.studenttest;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static String Name_Student, Surname_Student, Birthday_Date_Student;

    public static String[] ArrayGroup = {"P-1-17", "P-2-17", "P-3-17"};

    List<StudentClass> Students() {
        List<StudentClass> Students = new ArrayList<StudentClass>();
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));
//        Students.add(new StudentClass(R.drawable.ic_launcher_background, "Евгений", "Назаренко", "21.04.2001", "П-2-17"));

        return Students;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        setContentView(R.layout.item_layout);


        final CustomAdapter customAdapter = new CustomAdapter(this, Students());
        ((ListView) findViewById(R.id.ListView)).setAdapter(customAdapter);

        ((ListView) findViewById(R.id.ListView)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(MainActivity.this, InformationActivity.class);
                ImageView image = (ImageView) findViewById(R.id.PicOfStudent);
                intent.putExtra("id_picofstudent", ((BitmapDrawable)image.getDrawable()).getBitmap());
                intent.putExtra("name", ((TextView) view.findViewById(R.id.Name)).getText());
                intent.putExtra("surname", ((TextView) view.findViewById(R.id.Surname)).getText());
                intent.putExtra("birthday", ((TextView) view.findViewById(R.id.DateBirthday)).getText());
                intent.putExtra("group", ((TextView) view.findViewById(R.id.NumberOfGroup)).getText());
                intent.putExtra("selectedStudent", (StudentClass) customAdapter.getItem(i));
                startActivity(intent);
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ArrayGroup);
        ((Spinner) findViewById(R.id.ListGroup)).setAdapter(adapter);

    }

    public void AddClick(View view) {
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivityForResult(intent,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        List<StudentClass> students = new ArrayList<StudentClass>();

        students.add(new StudentClass((Bitmap)data.getExtras().get("imag"),data.getStringExtra("name"), data.getStringExtra("surname"), data.getStringExtra("birthday"),data.getStringExtra("group")));
    }
}
