package com.example.studenttest;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RegistrationActivity extends AppCompatActivity {

    public static String NameEnter, SurnameEnter, BirthdayEnter, Pol,SelectedGroup;
    public static Bitmap bitmapEnter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        String[] ArrayGroup = {"P-1-17", "P-2-17", "P-3-17"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ArrayGroup);
        ((Spinner) findViewById(R.id.Group)).setAdapter(adapter);
        Spinner spinner = findViewById(R.id.Group);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SelectedGroup=((TextView)view).getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void AddReg_Click(View view) {
        EditText NameET = findViewById(R.id.NameText);
        EditText SurnameET = findViewById(R.id.SurnameText);
        EditText BirthdayET = findViewById(R.id.DateBirth);
        RadioButton ManRB = findViewById(R.id.Man);
        RadioButton WomanRB = findViewById(R.id.Woman);




        Spinner spinner = findViewById(R.id.Group);

        Intent intententer = new Intent();
        //intententer.putExtra("id_picofstudent", ((ImageView) view.findViewById(R.id.Image)).getImageAlpha());
        intententer.putExtra("name", ((EditText) view.findViewById(R.id.NameText)).getText().toString());
        intententer.putExtra("surname", ((EditText) view.findViewById(R.id.SurnameText)).getText().toString());
        intententer.putExtra("birthday", ((EditText) view.findViewById(R.id.DateBirth)).getText().toString());
        intententer.putExtra("man", ((RadioButton) view.findViewById(R.id.Man)).getText());
        intententer.putExtra("woman", ((RadioButton) view.findViewById(R.id.Woman)).getText());
        intententer.putExtra("imag",bitmapEnter);
        intententer.putExtra("group",SelectedGroup);

        setResult(RESULT_OK, intententer);
        finish();


        /*





        StudentClass studentClass_objiect = (StudentClass) getIntent().getSerializableExtra("StudentEnter");
        int id_picofstudent = getIntent().getIntExtra("picofstudententer", 0);
        String name = getIntent().getStringExtra("nameenter");
        String surname = getIntent().getStringExtra("surnameenter");
        String birthday = getIntent().getStringExtra("birthdayenter");

        ((ImageView)findViewById(R.id.PicOfStudent)).setImageResource(studentClass_objiect.getId_PicOfStudent());
        ((TextView) findViewById(R.id.Name)).setText(name);
        ((TextView) findViewById(R.id.Surname)).setText(surname);
        ((TextView) findViewById(R.id.DateBirthday)).setText(birthday);
        ((TextView) findViewById(R.id.NumberOfGroup)).setText(group);

        Intent intent = new Intent();

        startActivity(intent);

        NameEnter = NameET.getText().toString();
        SurnameEnter = SurnameET.getText().toString();
        BirthdayEnter = BirthdayET.getText().toString();

        if (ManRB.isChecked()) {Pol = ManRB.getText().toString();}
        if (WomanRB.isChecked()) {Pol = WomanRB.getText().toString();}

            List<StudentClass> StudentsEnter = new ArrayList<StudentClass>();
        StudentsEnter.add(new StudentClass(bitmapEnter.getGenerationId(), NameEnter, SurnameEnter, BirthdayEnter, spinner.getSelectedItem().toString()));
    */
    }

    public void TakePhotoClick(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 3);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            ImageView imageView = findViewById(R.id.Image);
            imageView.setImageBitmap(bitmap);
            bitmapEnter = bitmap;
            //   Toast.makeText(this, "data", Toast.LENGTH_SHORT).show();

        } else {
            //   Toast.makeText(this, "datafail", Toast.LENGTH_SHORT).show();
        }
    }
}
